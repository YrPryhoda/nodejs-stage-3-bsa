const UserService = require('./userService');

class AuthService {
    login(userData) {
        const {email,password} = userData;
        const user = UserService.search({email,password});
        if (!user) {
            throw Error('User not found');
        }
        return user;
    }
}

module.exports = new AuthService();