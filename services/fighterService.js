const { FighterRepository } = require('../repositories/fighterRepository');
const { fighter } = require('../models/fighter');

class FighterService {
    // TODO: Implement methods to work with fighters
    //done
    create(fighterData) {
        const isExist = FighterRepository.getOne({ name: fighterData.name });
        if (isExist) {
            throw Error('Fighter already exists')
        }
        const newFighter = { ...fighter, ...fighterData };
        const item = FighterRepository.create(newFighter);
        if (!item) {
            throw Error('Server error')
        }
        delete item.id;
        return item;
    }
    findAll() {
        const items = FighterRepository.getAll();
        if (!items) {
            return null;
        }
        return items;
    }
    findById(id) {
        const item = FighterRepository.getOne({ id });
        if (!item) {
            throw Error('Fighter does not exist')
        }
        return item;
    }
    update(id, fighter) {
        const updatedFighter = FighterRepository.update(id, fighter);
        if (!updatedFighter) {
            throw Error('Could not update')
        }
        delete updatedFighter.id;
        return updatedFighter;
    }
    delete(id) {
        const item = FighterRepository.delete(id);
        if (!item.length) {
            throw Error('Fighter does not exist')
        }
        return item;
    }
}


module.exports = new FighterService();